const { request } = require("express");

const { response } = 'express';

const usuariosGet = (req = request, res = response) => {
   //const query = req.require;
 const { nombre, key = 'no key' } = req.query;
 res.json ({
   msg: 'get API -controlador',
   nombre,
   key
 });
  
};

const usuariosPut = (req, res = response) => {
  //const id = req.params.id;
  const { id } = req.params;

  res.json({
    msg: 'put API',
    id
  });


};

const usuariosPost = (req, res) => {

  //req.body trae la informacion del body
  const {nombre, edad} = req.body;

  res.status(201).json({
    msg: 'post API',
    nombre,
    edad
  });
}




const usuariosDelete = (req, res = response) => {
  res.json({
    msg: 'delete API - controlador'
  });
};

module.exports = {
  usuariosGet,
  usuariosPut,
  usuariosPost,
  usuariosDelete
}


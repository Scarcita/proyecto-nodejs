const { Router } = require('express');
const { usuariosGet, usuariosPut } = require('../controllers/user');

const router = Router();

router.get('/', usuariosGet);


router.put('/:id', usuariosPut);

router.post('/', (req, res) => {
  res.status(201).json({
    msg: 'post API'
  });
});
router.delete('/', (req, res) => {
  res.json({
    msg: 'delete API'
  });
});

module.exports = router;